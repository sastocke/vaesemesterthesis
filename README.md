# VAE Semester Thesis Sascha Stocker

##  Semster Project Sascha Stocker
Investigation of Variational Autoencoder (VAE) for Cardiovascular Magnetic Resonance (CMR) mask segmentation, semester project titled: Enhancement of the anatomical variability of human phantoms for cardiac imaging

The skelton framework, engine, trainig/valdiation epoch loop is from the tutorial on MNIST data:
https://debuggercafe.com/convolutional-variational-autoencoder-in-pytorch-on-mnist-dataset/ 

Results showed usability of VAEs for masks encoding with high DCE scores on the reconstruction even with multiple background labels.

## Results

<p align='center'>
  <img src='generated_images.gif ' width='10000'/>
</p>

## Directory Setup

Include an output folder in the same directory as the code files for the outputs to be saved in
Make sure the npzdata folder is also on the same line as the code files:

yourFolder/
- npzdata  
    - patient41.npz
    - patient42.npz
    - ...                                  
- outputs 
    - output1
    - output2
    - ...                                                                
- main.py
- training.py
- ...                         



# Files

## main.py
Our main file.

It is used to select the model used for training.
You select which data you want to use for training: LV mask, three channel background or full background.
It is also possible to inlcude or exclude the LV for the backgrounds.
Make sure your selection matches the meta data set in the meta_data file!

### Section 1: Model & Data selection
 Select your training purpose, for LV or background¨
 You can also load a trained model, make sure the trained model matches the meta_data set for the new train. Check the meta_data.txt file for comparison.
### Section 2: Training and Validtion
Training and validation of our model with the given data.

### Section 3: Image Generation


Calls the image generation from the image_geneartion_file

### Section 4: Latent Space Combination
*WORK IN PROGRESS*
Use the trained models to further use latenspace combination in model_combined.py

## meta_data.py
A file to set all meta data for the training setup. The values will be saved within the respective output folder as a meta_data.txt file to go along the results. This allows reproducability of your data and an the description of how the results were generated in the first place.

## model.py

Our VAE model

## model_combined.py

The combined model that is used for latent space combination.

## training.py
The file where the training and the validation occur.
It tracks the epochs and saves the model when it reaches the last epoch.
Also generates the first results for the last epochs after some inital training. If the first part of the training wants to be obsereved, one can take out the conditional on line 105
if(epoch >(epochs-5))
to see what the model trains in the beginning phase for investigation.

## engine.py
In the engine file we implemented the Binary Cross Entropy loss minimised during the training and validation.
Also the dice score for evalution is included.

## data.py
Functions to read out the data in the npz folder
The backgrounds can be generated to include or exclude the LV mask with the skip_lv boolean. 
For latent space combinations the LV should not be skipped.

## data_generation.py
File that is used to generate the new segmentation masks from the trained model.

## utils.py

Utility functions used throuhgout the other codes. Used for plotting results from dice scores train/validation loss, saving images of images.
Also saves the meta_data inputs into a text file.

To improve: instead of saving the dce scores as txt files one could make a csv file and directly show barplots.

## Old models Folder
Folder of the old models used during parametric study. We tested different layer sizes as well as dropout layers.
**Some updates will be needed to use the old models again**
- inputs to the class variables
- decoder function for the generation

Attention for the multi_class models, they use 10 layers and are very large in size. It might overload your GPU.

## Testing Scripts Folder
Scripts to run tests on the data. paths would need to be adjusted


## Issues:
- If the epoch size is too large and the server is busy, it can kill your process and no error message is run. Retry at another time when the server is less busy


## Authors and acknowledgment
Sascha Stocker, 
Dr. Stefano Buoso -  Supervisor
