# Imports
import numpy as np
import os
import matplotlib.image as mpimg
import torch
from torch.distributions import Normal
import torchvision
import matplotlib.pyplot as plt
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torchvision.utils import make_grid
import torchvision.transforms as transforms
from tqdm import tqdm
from torchvision.utils import save_image
import torchvision.datasets as datasets
from torch.utils.data import Dataset
import meta_data


trainingStart, trainingEnd = meta_data.get_data_range()

def loadTissueData(path_to_file):
    '''
    Read image intensity, multiclass-segmentations, and PD, T1, T2 (tissue properties)
    for multi-slice SAX images of the ACDC dataset
    dimensions are : [1,N_slices,Nx,Ny]

    all cases are have been previously resampled to 1x1 mm in-plane resolution and 10 mm slice thickness
    '''

    data = np.load(path_to_file)
    image = data['image']
    multiClass = data['multiClassMasks']
    PD = data['PD']
    T1 = data['T1']
    T2 = data['T2']

    return image,multiClass,PD,T1,T2

def loadLVOnly(path_to_file):
    '''
    Read only LV segmentation masks
    '''

    multiClass = np.load(path_to_file)['multiClassMasks']
    LVMask = 1*(multiClass == 3)
    return LVMask


def get_training_data():
    """
    Retrieves the data for the LV data of patient 65-100 for training data
    
    Inputs:
    - none
    
    Output: the multichannel data as a torch float32 tensor in the shape of [nr_images, channels, image_height, image_width]
    nr_images = variable depending on start and end of training data

    for LV  channels = 1
    image_height = image_width = 256
    """

    LVMaskTrain = loadLVOnly(f'./npzdata/patient065.npz')
    numberOfImages=(LVMaskTrain.shape[0] * LVMaskTrain.shape[1])
    LVMaskTrain = LVMaskTrain.reshape(numberOfImages, 256, 256)


    for i in range(trainingStart,trainingEnd):
        # skip patient 84 and 88, non existent
        if(i==84 or i == 88):
            continue
            
        LVMask = loadLVOnly(f'./npzdata/patient0{i}.npz')
    
        #first two entries are time and depth slices, reshape use them all as individual images
        numberOfImages=(LVMask.shape[0] * LVMask.shape[1])
        
        
        LVMask = LVMask.reshape(numberOfImages,256,256)
        LVMaskTrain =np.concatenate((LVMaskTrain,LVMask))
    
    #shuffle the data to exlcude time dependance of the data
    np.random.shuffle(LVMaskTrain)
    LVMaskTrain = torch.from_numpy(LVMaskTrain)
    LVMaskTrain = LVMaskTrain.type(torch.float32)
    #add a channel dimension for pytorch training is prepared with channel sizes
    LVMaskTrain = torch.unsqueeze(LVMaskTrain,1)
    
    
    return LVMaskTrain

def get_test_data():
    """ 


    Retrieves the data for the LV data of patient 41 47 and 52 for test data 

    Inputs:
    - none
    
    Output: the multichannel data as a torch float32 tensor in the shape of [nr_images, channels, image_height, image_width]
    for LV  channels = 1
    
    """

    LVMaskTest = loadLVOnly(f'./npzdata/patient041.npz')
    numberOfImages=(LVMaskTest.shape[0] * LVMaskTest.shape[1])
    LVMaskTest = LVMaskTest.reshape(numberOfImages, 256, 256)


    for i in (41,47,52,53,55,57,58,60,65):
        # skip patient 84 and 88, non existent
        if(i==84 or i == 88):
            continue
            
        LVMask = loadLVOnly(f'./npzdata/patient0{i}.npz')
    
        #first two entries are time and depth slices, reshape use them all as individual images
        numberOfImages=(LVMask.shape[0] * LVMask.shape[1])
        
        
        LVMask = LVMask.reshape(numberOfImages,256,256)
        LVMaskTest =np.concatenate((LVMaskTest,LVMask))
    
    #shuffle the data to exlcude time dependance of the data
    np.random.shuffle(LVMaskTest)
    LVMaskTest = torch.from_numpy(LVMaskTest)
    LVMaskTest = LVMaskTest.type(torch.float32)
    #add a channel dimension for pytorch training is prepared with channel sizes
    LVMaskTest = torch.unsqueeze(LVMaskTest,1)

    return LVMaskTest

def load_mask(path_to_file, i):
    '''
    Read out channel i only from the data

    Inputs:
    - path_to_file: path to the data file
    - i: channel index

    Output: np array of shape [time_slices, z_sclies, width, height]
    - width = height = 256
    - time_slices and z_slices are variable for different patient data but consistent throught all channels for a given patient
    '''

    multiClass = np.load(path_to_file)['multiClassMasks']
    mask = 1*(multiClass == i)
    return mask


def get_multichannel_data(channels,skip_lv):
    """ Retrieves the data for the multi-channel background data

    Input:

    - channels, the number of channels to get for the multichannel data !
    channel = 12 means all background channels are included
    channel = 3 means only the 3 background channels are included (left and right blood pool and the right ventricle wall)
    - skip_lv, boolean, if true the LV channel is not included in the multichannel data

ATTENTION: the LV is specifically skipped if skip_lv is set to True
So channels = 12 will generate 11 channels of background and channels = 3 will generate 3 channels of background as the LV at position 3 is skipped
Set the meta_data.py file to the correct number of channels !!

    Output: The connected data for the multi-channel background as a torch float32 tensorin the shape of [nr_images, channels, image_height, image_width]
    nr_images = depending on the amount of data selected with the trainingStart and trainingEnd variables
    channels = depending on the channels variable
    image_height = 256
    image_width = 256

    """


    for i in range (trainingStart,trainingEnd):

        #missing patient 84 and 88
        if(i==84 or i == 88):
            continue
        path = f'./npzdata/patient0{i}.npz'


        #take in first data
        multiChannel = load_mask(path,0)

        #reshape the slices and time to single images  ( ASK AGAIN!)
        nrOfImages = multiChannel.shape[0] * multiChannel.shape[1]

        #resahpe to prepare for concatenation
        multiChannel = multiChannel.reshape(nrOfImages,1, 256, 256)


        for j in range(1,channels):

            # exclude the LV to only get background!
            if(j==3 and skip_lv):
                continue
            channel = load_mask(path, j)

            channel = channel.reshape(nrOfImages,1, 256, 256)

            #concatenate along channels
            multiChannel = np.concatenate((multiChannel, channel), axis=1)

        if(i==trainingStart):
            allMultiChannel = multiChannel
        else:
            #concatenate along the images of the patients
            allMultiChannel= np.concatenate((allMultiChannel, multiChannel), axis=0)
            
            

    #shuffle data to exclude the time dependence of the generated images that persits in the data
    #if the images are just concatenated (in the original form they come as slices | time , and we just flatten them)
    np.random.shuffle(allMultiChannel)

    # turn numpy into torch tensor for pytorch training
    allMultiChannel = torch.from_numpy(allMultiChannel)
    allMultiChannel = allMultiChannel.type(torch.float32)
    

    print(f'All multi channel shape: {allMultiChannel.shape}')

    return allMultiChannel



def get_multichannel_testdata(channels, skip_lv):
    """Retrieve the data for the multi-channel model same as the get_multichannel_data() function but for the test data

    Returns: The connected data for the multi-channel model in the shape of [channel, nr_images, image_height, image_width]
    
    Check the get_multichannel_data() function for more information! """

    # Test patients are chosen to be the ones that do not follow numberical sequence
    for i in (41,47,52,53,55,57,58,60,65):
        path = f'./npzdata/patient0{i}.npz'


        #take in first data, just the LV mask
        
        multiChannel = load_mask(path,0)

        #reshape the slices and depths to single images
        nrOfImages = multiChannel.shape[0] * multiChannel.shape[1]

        #resahpe to prepare for concatenation
        multiChannel = multiChannel.reshape(nrOfImages,1, 256, 256)


        for j in range(1,channels):
            #exlcude the LV mask!
            if (j==3 and skip_lv):
                continue
            channel = load_mask(path, j)

            channel = channel.reshape(nrOfImages, 1,256, 256)

            #concatenate along channels
            multiChannel = np.concatenate((multiChannel, channel), axis=1)

        if(i==41):
            allMultiChannel = multiChannel
        else:
            #concatenate along different patient datas
            allMultiChannel= np.concatenate((allMultiChannel, multiChannel), axis=0)

        

    # turn numpy into torch tensor
    np.random.shuffle(allMultiChannel)
    allMultiChannel = torch.from_numpy(allMultiChannel)
    allMultiChannel = allMultiChannel.type(torch.float32)
    

    print(f'All multi channel shape: {allMultiChannel.shape}')

    return allMultiChannel







def get_multichannel_data_single():
    """ Retrieves the data for the multi-channel background data

    nr_images = depending on the amount of data selected with the trainingStart and trainingEnd variables in the meta_data.py file
    channels = 11 , only background the backgrounds, specifically excludes the LV mask
    image_height = 256
    image_width = 256
    
    Returns: The connected data for the multi-channel model in the shape of [nr_images, channels, image_height, image_width]
    

    """

    i = True

    #missing patient 84 and 88
    path = f'./npzdata/patient067.npz'


    #take in first data
    multiChannel = load_mask(path,0)

    #reshape the slices and time to single images  ( ASK AGAIN!)
    nrOfImages = multiChannel.shape[0] * multiChannel.shape[1]

    #resahpe to prepare for concatenation
    multiChannel = multiChannel.reshape(nrOfImages,1, 256, 256)


    for j in range(1,12):

        # exclude the LV to only get background!
        if(j==3):
            continue
        channel = load_mask(path, j)

        channel = channel.reshape(nrOfImages,1, 256, 256)

        #concatenate along channels
        multiChannel = np.concatenate((multiChannel, channel), axis=1)

    if(i==True):
        allMultiChannel = multiChannel
        i=False
    else:
        #concatenate along the images of the patients
        allMultiChannel= np.concatenate((allMultiChannel, multiChannel), axis=0)
            
            

    #shuffle data to exclude the time dependence of the generated images that persits in the data
    #if the images are just concatenated (in the original form they come as slices | time , and we just flatten them)
    np.random.shuffle(allMultiChannel)

    # turn numpy into torch tensor
    allMultiChannel = torch.from_numpy(allMultiChannel)
    allMultiChannel = allMultiChannel.type(torch.float32)
    

    print(f'All multi channel shape: {allMultiChannel.shape}')

    return allMultiChannel[0:10,:,:,:]



def get_multichannel_testdata_single():
    """Retrieve the data for the multi-channel model same as the get_multichannel_data() function but for the test data

    Returns: The connected data for the multi-channel model in the shape of [channel, nr_images, image_height, image_width] """

#41 47 52
    i = True
    path = f'./npzdata/patient041.npz'


    #take in first data, just the LV mask
    
    multiChannel = load_mask(path,0)

    #reshape the slices and depths to single images
    nrOfImages = multiChannel.shape[0] * multiChannel.shape[1]

    #resahpe to prepare for concatenation
    multiChannel = multiChannel.reshape(nrOfImages,1, 256, 256)


    for j in range(1,12):
        #exlcude the LV mask!
        if (j==3):
            continue
        channel = load_mask(path, j)

        channel = channel.reshape(nrOfImages, 1,256, 256)

        #concatenate along channels
        multiChannel = np.concatenate((multiChannel, channel), axis=1)

    if(i==True):
        allMultiChannel = multiChannel
        i=False
    else:
        #concatenate along different patient datas
        allMultiChannel= np.concatenate((allMultiChannel, multiChannel), axis=0)

        

    # turn numpy into torch tensor
    np.random.shuffle(allMultiChannel)
    allMultiChannel = torch.from_numpy(allMultiChannel)
    allMultiChannel = allMultiChannel.type(torch.float32)
    

    print(f'All multi channel shape: {allMultiChannel.shape}')

    return allMultiChannel[0:10,:,:,:]




### bad approach as the data formated like this does not take into account the dependence of the channels!!!
"""
def get_multichannel_background():
     Retrieve the data for the multi-channel model but only the background
    
    Returns: The connected data for the multi-channel model in the shape of [channel, nr_images, image_height, image_width]


    for i in range (trainingStart,trainingEnd):
        if(i==84 or i == 88):
            continue
        path = f'./npzdata/patient0{i}.npz'


        #take in first data, just the LV mask
        
        multiChannel = load_mask(path,2)

        #reshape the slices and depths to single images
        nrOfImages = multiChannel.shape[0] * multiChannel.shape[1]

        #resahpe to prepare for concatenation
        multiChannel = multiChannel.reshape(nrOfImages, 256, 256)


        for j in range(3,12):
            channel = load_mask(path, j)

            channel = channel.reshape(nrOfImages, 256, 256)

            #concatenate along channels
            multiChannel = np.concatenate((multiChannel, channel), axis=0)

        if(i==trainingStart):
            allMultiChannel = multiChannel
        else:
            #concatenate along different patient datas
            allMultiChannel= np.concatenate((allMultiChannel, multiChannel), axis=0)

        
    
    # turn numpy into torch tensor
    allMultiChannel = torch.from_numpy(allMultiChannel)
    allMultiChannel = allMultiChannel.type(torch.float32)
    allMultiChannel =torch.unsqueeze(allMultiChannel,1)
    print(f'All multi channel shape: {allMultiChannel.shape}')

    return allMultiChannel

def get_multichannel_background_testdata():
    Retrieve the data for the multi-channel model

    Returns: The connected data for the multi-channel model in the shape of [channel, nr_images, image_height, image_width]


    for i in (41,47,52):
        path = f'./npzdata/patient0{i}.npz'


        #take in first data, just the LV mask
        
        multiChannel = load_mask(path,2)

        #reshape the slices and depths to single images
        nrOfImages = multiChannel.shape[0] * multiChannel.shape[1]

        #resahpe to prepare for concatenation
        multiChannel = multiChannel.reshape(nrOfImages, 256, 256)


        for j in range(3,12):
            channel = load_mask(path, j)

            channel = channel.reshape(nrOfImages,256, 256)

            #concatenate along channels
            multiChannel = np.concatenate((multiChannel, channel), axis=0)

        if(i==41):
            allMultiChannel = multiChannel
        else:
            #concatenate along different patient datas
            allMultiChannel= np.concatenate((allMultiChannel, multiChannel), axis=0)

        

    # turn numpy into torch tensor
    allMultiChannel = torch.from_numpy(allMultiChannel)
    allMultiChannel = allMultiChannel.type(torch.float32)
    allMultiChannel =torch.unsqueeze(allMultiChannel,1)

    print(f'All multi channel shape: {allMultiChannel.shape}')

    return allMultiChannel

"""