import torch
import torch.optim as optim
import torch.nn as nn

### Setup for training of LV
def get_training_LV_setup():
    
    lr = 0.001
    epochs = 50
    batch_size = 30
    return lr, epochs, batch_size

### Setup for training multi class background
def get_training_background_setup():
    
    lr = 0.0001
    epochs = 50
    batch_size = 30
    return lr, epochs, batch_size



### Setup for the model
def get_model_setup():
    # latent space
    kernel_size = 4 # (4, 4) kernel
    #lower the network size by decreaseing init_channels
    # Init Channels have to be larger than 1 for the model to work
    init_channels = 2 # initial number of filters
    image_channels = 1 #MNIST images are grayscale
    latent_dim = 16 # latent dimension for sampling
    dropout = 0.2
    # image parameters
    return kernel_size, init_channels, image_channels, latent_dim, dropout


### Setup for the engine

def get_engine_setup():
    B=0.1
    return B# Factor to weigh between reconstruction loss and KL divergence loss


def get_data_range():
    training_start =67
    training_end = 100
    return training_start, training_end


def get_multiclass_model_setup():
    # latent space
    kernel_size = 4 # (4, 4) kernel
    # Init Channels have to be larger than 1 for the model to work

    init_channels = 4# initial number of filters
    image_channels =3 #11 channeled backgrounds or 3 for 
    latent_dim = 30 # latent dimension for sampling
    dropout = 0.5
    # image parameters
    return kernel_size, init_channels, image_channels, latent_dim,dropout

def get_image_generation_parameter():
    mean= 0
    std = 2
    return mean, std