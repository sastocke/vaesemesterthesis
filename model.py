import torch
import torch.nn as nn
import torch.nn.functional as F
import meta_data

# define a Conv VAE
class ConvVAE(nn.Module):
    def __init__(self,kernel_size,init_channels,image_channels,latent_dim,dropout):
        super(ConvVAE, self).__init__()
        self.kernel_size = kernel_size
        self.init_channels = init_channels
        self.image_channels = image_channels
        self.latent_dim = latent_dim
        self.dropout = dropout

 
        # encoder
        self.enc1 = nn.Conv2d(
            in_channels=image_channels, out_channels=init_channels, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.enc2 = nn.Conv2d(
            in_channels=init_channels, out_channels=init_channels*2, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.enc3 = nn.Conv2d(
            in_channels=init_channels*2, out_channels=init_channels*4, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.enc4 = nn.Conv2d(
            in_channels=init_channels*4, out_channels=init_channels*8, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.enc5 = nn.Conv2d(
            in_channels=init_channels*8, out_channels=init_channels*16, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.enc6 = nn.Conv2d(
            in_channels=init_channels*16, out_channels=init_channels*32, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.enc7 = nn.Conv2d(
            in_channels=init_channels*32, out_channels=256, kernel_size=kernel_size, 
            stride=2, padding=0
        )
        # fully connected layers for learning representations
        
        self.fc1 = nn.Linear(256, 512)

        # two fully connected layers are responsible for providing us the mean and log variance value from this bottleneck part.
        # We will do the sampling using these features and in-turn this will lead to the reconstruction of the images. 
        self.fc_mu = nn.Linear(512, latent_dim)
        self.fc_log_var = nn.Linear(512, latent_dim)
        self.fc2 = nn.Linear(latent_dim, 256)
        # decoder, simply opposite of the encoder part
        self.dec1 = nn.ConvTranspose2d(
            in_channels=256, out_channels=init_channels*64, kernel_size=kernel_size, 
            stride=1, padding=0
        )
        self.dec2 = nn.ConvTranspose2d(
            in_channels=init_channels*64, out_channels=init_channels*32, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.dec3 = nn.ConvTranspose2d(
            in_channels=init_channels*32, out_channels=init_channels*16, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.dec4 = nn.ConvTranspose2d(
            in_channels=init_channels*16, out_channels=init_channels*8, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.dec5 = nn.ConvTranspose2d(
            in_channels=init_channels*8, out_channels=init_channels*4, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.dec6 = nn.ConvTranspose2d(
            in_channels=init_channels*4, out_channels=init_channels*2, kernel_size=kernel_size, 
            stride=2, padding=1
        )
        self.dec7 = nn.ConvTranspose2d(
            in_channels=init_channels*2, out_channels=image_channels, kernel_size=kernel_size, 
            stride=2, padding=1
        )
    def reparameterize(self, mu, log_var):
        """
        :param mu: mean from the encoder's latent space
        :param log_var: log variance from the encoder's latent space
        """
        std = torch.exp(0.5*log_var) # standard deviation
        eps = torch.randn_like(std) # `randn_like` as we need the same size
        sample = mu + (eps * std) # sampling
        return sample
 
    def forward(self, x):
        # encoding

        #print(f'x shape beginning of forward: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc1(x))
        #print(f'x shape after enc1: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc2(x))
        #print(f'x shape after enc2: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc3(x))
        #print(f'x shape after enc3: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc4(x))
        #print(f'x shape after enc4: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc5(x))
        #print(f'x shape after enc5: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc6(x))
        #print(f'x shape after enc6: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.enc7(x))
        #print(f'x shape after enc7: {x.shape}, x type: {x.dtype}')

        #print(f'x shape after encoding: {x.shape}, x type: {x.dtype}')
        batch, _, _, _ = x.shape
        x = F.adaptive_avg_pool2d(x, 1).reshape(batch, -1)
        hidden = self.fc1(x)
        #print(f'hidden shape after fc1: {hidden.shape}, hidden type: {hidden.dtype}')
        # get `mu` and `log_var`
        mu = self.fc_mu(hidden)
        log_var = self.fc_log_var(hidden)
        # get the latent vector through reparameterization
        z = self.reparameterize(mu, log_var)
        #print(f'z shape after reparameterize: {z.shape}, z type: {z.dtype}')
        #print(f'z shape in latent space: {z.shape}, z type: {z.dtype}')
        z = self.fc2(z)
        #print(f'z shape after fc2: {z.shape}, z type: {z.dtype}')

        # first entry has to be -1, because we don't know the batch size for edge cases.
        # the other entries are the dimensions of the tensor last encoded by the encoder
        z = z.view(-1, 256, 1, 1)
        #print(f'z shape after view manipulation: {z.shape}, z type: {z.dtype}')

        #print(f'z shape beginning decoding: {z.shape}, z type: {z.dtype}')

        # decoding
        x = F.relu(self.dec1(z))
        #print(f'x shape after dec1: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec2(x))
        #print(f'x shape after dec2: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec3(x))
        #print(f'x shape after dec3: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec4(x))
        #print(f'x shape after dec4: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec5(x))
        #print(f'x shape after dec5: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec6(x))
        #print(f'x shape after dec6: {x.shape}, x type: {x.dtype}')
    
        #print(f'x shape at end of decoding: {x.shape}, x type: {x.dtype}')

        
        reconstruction = torch.sigmoid(self.dec7(x))

        z = self.reparameterize(mu, log_var)

        #print(f'reconstruction shape for return: {reconstruction.shape}, reconstruction type: {reconstruction.dtype}')
        return reconstruction, mu, log_var,z


    def get_latenspace(self ,z):

        # run over the images we have -> for each latent space plot the histogram of the latent space
        # check how far you are away from a normal distribution. (will not be exactly normal but should be close)
        # tune beta to get to a normal distribution
        # run on a loop -> to get N latent variables
        # plot the histogram of the latent space for each element of the latent space vector for each iamge
        # should be a normal distribution
        z = self.fc2(z)
        x = F.relu(self.enc1(x))
        x = F.relu(self.enc2(x))
        x = F.relu(self.enc3(x))
        x = F.relu(self.enc4(x))
        x = F.relu(self.enc5(x))
        x = F.relu(self.enc6(x))
        x = F.relu(self.enc7(x))

        #print(f'x shape after encoding: {x.shape}, x type: {x.dtype}')
        batch, _, _, _ = x.shape
        x = F.adaptive_avg_pool2d(x, 1).reshape(batch, -1)
        hidden = self.fc1(x)
        # get `mu` and `log_var`
        mu = self.fc_mu(hidden)
        log_var = self.fc_log_var(hidden)
        # get the latent vector through reparameterization
        return mu, log_var

    def decode_from_latentspace(self,z):
        """ Decode a latent space vector to an image 

        Input:
        - x: latent space vector
        
        Output:
        - reconstruction: reconstructed image
        """
        z = self.fc2(z)
        z = z.view(-1, 256, 1, 1)
        x = F.relu(self.dec1(z))
        #print(f'x shape after dec1: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec2(x))
        #print(f'x shape after dec2: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec3(x))
        #print(f'x shape after dec3: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec4(x))
        #print(f'x shape after dec4: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec5(x))
        #print(f'x shape after dec5: {x.shape}, x type: {x.dtype}')
        x = F.relu(self.dec6(x))
        #print(f'x shape after dec6: {x.shape}, x type: {x.dtype}')
    
        #print(f'x shape at end of decoding: {x.shape}, x type: {x.dtype}')

        
        reconstruction = torch.sigmoid(self.dec7(x))
        return reconstruction
