from multiprocessing.spawn import import_main_path
from re import I
import torch
import torch.optim as optim
import torch.nn as nn
import matplotlib
from torch.utils.data import DataLoader
from torchvision.utils import make_grid
from engine import train, validate
import os

# utilitiy functions for plotting and viewing different aspects of our model
from utils import save_reconstructed_images,image_to_vid, save_reconstructed_multi_images,overlay_images
matplotlib.style.use('ggplot')

# import the functions to get the data and meta data for training / testing
import torchvision.transforms as transforms
import meta_data 
print('starting')
torch.cuda.empty_cache()
import matplotlib.pyplot as plt


def train_model(model,training_data,test_data,epochs,lr,batch_size,device,latent_dim,image_channels,z_lv):
    optimizer = optim.Adam(model.parameters(), lr=lr)
    criterion = nn.BCELoss(reduction='sum')

    # Make sure the image is transformed to the correct data shape
    transform = transforms.Compose([
        transforms.Resize((256,256)),
        transforms.ToTensor(),
    ])

    # creating the dataloader for training and testing
    trainloader = DataLoader(
        training_data, batch_size=batch_size, shuffle=True
    )
    testloader = DataLoader(
        test_data, batch_size=batch_size, shuffle=False
    )

    # Running losses and dice scores for training and testing
    train_loss = []
    valid_loss = []
    #declaring empty array of sizes image_channel, epochs
    #dice_scores_training = np.zeros((image_channels,epochs))
    #dice_scores_validation = np.zeros((image_channels,epochs))

    dce_score_training = torch.zeros(image_channels,epochs)
    dce_score_validation = torch.zeros(image_channels,epochs)
    z_all= torch.zeros((1,latent_dim)).type(torch.float32)
    

    folder = 1


    

    # Training loop

    #grid_images used for the plots
    grid_images = []
    for epoch in range(epochs):
        print(f"Epoch {epoch+1} of {epochs}")
        if (epoch==0):
            print("checking for folder")
            print(f'original path exists: {os.path.exists("./output")}')
            while(os.path.exists(f'./outputs/output{folder}')):
                folder +=1

            print(f"creating folder: ./outputs/output{folder}")
            os.makedirs(f'./outputs/output{folder}')
            
            

        #saving the trainde model-
        if(epoch == epochs-1):
            torch.save(model.state_dict(), f'./outputs/output{folder}/{type(model)}{epoch}.pth')
        
        train_epoch_loss,z_batch, train_dce = train(
            model, trainloader, training_data, device, optimizer, criterion, z_lv
        )
        valid_epoch_loss, recon_images,val_dce,original_images = validate(
            model, testloader, test_data, device, criterion,
        z_lv)
        train_loss.append(train_epoch_loss)
        valid_loss.append(valid_epoch_loss)
        dce_score_training[:,epoch] += train_dce[:,0]
        dce_score_validation[:,epoch] +=val_dce[:,0]
        # After sufficient training, check what the reconstructions look like
        if (image_channels> 1 and epoch >(epochs-3)):
            print(f' multiclass recon recon_image is saved')
            save_reconstructed_multi_images(recon_images,original_images, epoch+1, folder,batch_size)
            
        # After finishing training, check if the multichannel does reconstruct viable backgrounds from random latentspace

        elif(image_channels ==1):
            save_reconstructed_images(recon_images, epoch+1,folder)
            image_grid = make_grid(recon_images.detach().cpu())
            grid_images.append(image_grid)
            image_to_vid(grid_images,folder)
            
            
            if(epoch >(epochs-5)):
                overlay_images(recon_images, original_images,int(batch_size), epoch+1,folder,False, 1)

                
        
        z_all = torch.concat((z_all.to(device),z_batch.to(device)),0)
        

        

        

        #print(f"Train Loss: {train_epoch_loss:.4f}")
        #print(f"Val Loss: {valid_epoch_loss:.4f}")

    return train_loss,valid_loss,dce_score_training,dce_score_validation,z_all,folder