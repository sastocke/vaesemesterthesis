from tqdm import tqdm
import torch.nn as nn

import torch 

import meta_data

# Declaring empty latent space reprensentation


B= meta_data.get_engine_setup()


def final_loss(loss, mu, logvar):
    """
    This function will add the reconstruction loss (BCELoss) and the 
    KL-Divergence. Tuned by the hyperparameter 'B' higher values giving more structured latent space at the cost of poorer reconstruction

    https://stats.stackexchange.com/questions/332179/how-to-weight-kld-loss-vs-reconstruction-loss-in-variational-auto-encoder
    https://openreview.net/forum?id=Sy2fzU9gl 

    KL-Divergence = 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    
    Inputs:
    - loss: from the images reconstructed by the VAE NN and original data
    - mu: the mean from the latent vector
    -logvar: log variance from the latent vector

    Returns:
    - final_loss: the sum of the reconstruction loss and the B-weighted KL-Divergence
    """
    ## CAN PUT IN PARAMETER WEIGHTS FOR THESE TWO LOSSES! B-variational autoencoder! Check papers!
    # -> tune B

    #accuracy in reconstruction 
    #similarity with normal distribution
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
    return (loss +  B * KLD)

def dice_score( inputs, targets, smooth=1):
    """ Dice score for the validation step of the model 

    adapted from https://www.kaggle.com/code/bigironsphere/loss-function-library-keras-pytorch/notebook#Dice-Loss 
    where Dice loss is defined as 1 - dice score -> only the dice score is returned

    Inputs:
    - inputs: the predicted segmentation
    - targets: the ground truth segmentation
    - smooth: smoothing factor to avoid division by zero

    Returns:
    - dice: the dice score for the predicted segmentation and the ground truth segmentation


    
    """
        
    #flatten label and prediction tensors
    inputs = inputs.flatten(-1)
    targets = targets.flatten(-1)
    
    intersection = (inputs * targets).sum()                            
    dice = (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
    
    return dice

def train(model, dataloader, dataset, device, optimizer, criterion,z_lv):
    torch.autograd.set_detect_anomaly(True)

    """
    Training of the model
    
    Inputs:
    
    - Variational Auteoncoder model
    - dataloader for the training data
    - dataset for the training data
    - device to run the model on
    - optimizer for the model
    - loss function for the model
    - z_lv: latent vector to be used for the model, can be None, if None then the model will be trained without a latent vector

    Returns: 
    - train_loss: training loss as an array, carrying the loss values for each batch fro the criterion, evaluated cross entropy on the reconstruction and the training data

    

    """
    model.train()
    model.to(device)
    #torch variable of image channels size

    image_channels = dataset.shape[1]

    running_training_dce = torch.zeros(image_channels,1)


    #keeping track of the batch-wise loss values while training
    running_loss = 0.0

    #track total number of training steps
    counter = 0
    for i, data in tqdm(enumerate(dataloader), total=int(len(dataset)/dataloader.batch_size)):
        counter += 1
        #print(f"Data when feeding into training: Data shape: {data.shape}, type is: {type(data)} and torch type is: {data[0][0].dtype}")
        data = data.to(device)

        optimizer.zero_grad()
        if(z_lv != None):

            reconstruction, mu, logvar,z = model(data,z_lv, dataloader.batch_size)
        else:
            reconstruction, mu, logvar,z = model(data)


        #BCE loss for loss comperison of training loss
        bce = criterion(reconstruction, data)

        #final loss is the sum of the BCE loss and the KL divergence
        loss = final_loss(bce, mu, logvar)

        #Set retain_graph = True to avoid memory leak, tried in combined model, but still didn't work
        #loss.backward(retain_graph=True)
        loss.backward()

        
        
        reconstructed_rounded = torch.round(reconstruction)
        

        for j in range(reconstructed_rounded.shape[1]):
            dce = dice_score(reconstructed_rounded[:,j,:,:], data[:,j,:,:])
            running_training_dce[j] += dce.item()

        running_loss += loss.item()
        optimizer.step()
        if (counter == 1):
            z_batch=z
        else:
            z_batch=torch.cat((z_batch,z),0)
        #print(f'z shape in call from reconstruction: {z.shape}, type is: {type(z)}')
        #print(f'running_loss: {running_loss}')s
    train_loss = running_loss / counter 
    train_dce = running_training_dce / counter
    #print(f'shape of training dce: {train_dce.shape}, values of training dce: {train_dce}')
    return train_loss, z_batch, train_dce

def validate(model, dataloader, dataset, device, criterion, z_lv):
    torch.autograd.set_detect_anomaly(True)


    """ Validation step of the model.
    
    Inputs: 
    - model: Auteoncoder model
    - dataloader: dataloader for the validation data
    - dataset: dataset for the validation data
    - device: device to run the model on
    - criterion: loss function for the model
    - z_lv: latent vector to be used for the model, can be None, if None then the model will be trained without a latent vector
    
    Returns:
    - val_loss: validation loss as an array, carrying the loss values for each batch
    - recon_images: reconstructed images from the validation data
    - val_dce: validation dce score on rounded reconstructed images and original images
    - original: original images that were used for validation
    
    """
    model.eval()
    model.to(device)
    running_loss = 0.0
    image_channels = dataset.shape[1]
    running_val_dce = torch.zeros(image_channels,1)
    counter = 0
    recon_images=None
    with torch.no_grad():
        for i, data in tqdm(enumerate(dataloader), total=int(len(dataset)/dataloader.batch_size)):
            counter += 1
            data = data.to(device)
            if(z_lv != None):
                reconstruction, mu, logvar,z = model(data,z_lv,dataloader.batch_size)
            else:
                reconstruction, mu, logvar, z= model(data)

            #use BCE loss for loss comperison of training loss and validation loss but include the dice alone to see how well the model is reconstruction our data
            bce = criterion(reconstruction, data)
            loss = final_loss(bce, mu, logvar)

            #dce score calculated on the rounded reconstruction and the original data
            reconstructed_rounded = torch.round(reconstruction)
            #print(f'reconstructed_rounded shape: {reconstructed_rounded.shape}, type is: {type(reconstructed_rounded)} and torch type is: {reconstructed_rounded[0][0].dtype}')
            for j in range(reconstructed_rounded.shape[1]):
                dce = dice_score(reconstructed_rounded[:,j,:,:], data[:,j,:,:])
                running_val_dce[j] += dce.item()

            #print(f'dce: {dce}, shape: {dce.shape}, type: {type(dce)}')
            #print(f'loss: {loss}')
            running_loss += loss.item()

            
            # save the last fully sized batch input and output of every epoch
            if i == int(len(dataset)/dataloader.batch_size) - 1:
                recon_images = reconstruction
                original_images = data
                #print(f'recon_image has been produced')


    val_loss = running_loss / counter
    val_dce = running_val_dce / counter
    #print(f'shape of val_loss: {val_loss}')
    #print(f'shape of val_dce: {val_dce}')


    return val_loss, recon_images, val_dce, original_images