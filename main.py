import torch
import model
import matplotlib
from torchvision import models
from torchsummary import summary

# utilitiy functions for plotting and viewing different aspects of our model
from utils import meta_data_used, save_loss_plot, plot_latent_representation_distribution,save_dce_plot, max_dice
matplotlib.style.use('ggplot')
# import the functions to get the data and meta data for training / testing
from data import get_test_data,get_training_data, get_multichannel_data, get_multichannel_testdata
import torchvision.transforms as transforms
#import multiclass_model
#import multiclass_model_dropout
import meta_data 
#import model_dropout
import model_combined
print('starting')
torch.cuda.empty_cache()
from training import train_model

from  image_generation import image_generation



##############################################################################################
#
#                           Section 1: Model and Data Selection
#
###############################################################################################


# Set up which model you want to train

training = "background"
#training = "trained_model"
#training = "background"


# Set the device to be used for training
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")



if(training == 'background'):
    # Set meta_data.py to use the background data for either 11 channel or 3 channel!
    print("background training")
    lr, epochs, batch_size = meta_data.get_training_background_setup()
    kernel_size, init_channels, image_channels, latent_dim,dropout = meta_data.get_multiclass_model_setup()
    model = model.ConvVAE(kernel_size, init_channels, image_channels, latent_dim,dropout ).to(device)
    
    #get the training and test data, set if you want to skip the LV channel. For background only, set skip_LV to True
    training_data = get_multichannel_data(3,skip_lv=True)
    test_data = get_multichannel_testdata(3,skip_lv=True)

elif(training == 'LV'):
    
    print("LV training")
    lr, epochs, batch_size = meta_data.get_training_LV_setup()
    kernel_size, init_channels, image_channels, latent_dim, dropout= meta_data.get_model_setup()
    model = model.ConvVAE(kernel_size, init_channels, image_channels, latent_dim,dropout).to(device)

    training_data = get_training_data()
    test_data = get_test_data()

# For pretrained model make sure all the meta data and training data are set to the pretrained model one!
# Check the meta_data.txt file for the correct values of the pretrained model
elif(training == 'trained_model'):
    lr, epochs, batch_size = meta_data.get_training_background_setup()
    kernel_size, init_channels, image_channels, latent_dim,dropout = meta_data.get_multiclass_model_setup()
    model = model.ConvVAE().to(device)
    model = model.ConvVAE("path to the model")
    training_data = get_multichannel_data(3)
    test_data = get_multichannel_testdata(3)



##############################################################################################
#
#                           Section 2: Training and Validation
#
###############################################################################################

#Output of the model and its layers

#summary(model,(3,256,256))
#training and validation the model
train_loss, valid_loss, dce_score_training, dce_score_validation,z_all,folder = train_model(model,training_data,test_data,epochs,lr,batch_size,device,latent_dim,image_channels, None)

print(f'TRAINING COMPLETE')

            
#Plotting the latent representation distribution, see if the latent representation is distributed normally
plot_latent_representation_distribution(z_all,folder)
save_loss_plot(train_loss, valid_loss,folder)
save_dce_plot(dce_score_validation,folder,False)
save_dce_plot(dce_score_training,folder,True)
max_dice(dce_score_validation,folder,train = False)
max_dice(dce_score_training,folder,train=True)

# Save the metadata that was used for training
meta_data_used(folder)
print(f'PLOTS COMPLETE')



##############################################################################################
#
#                           Section 3: Image Generation
#
###############################################################################################

#Generation of new image for trained model and random latent representation set in meta data
print("Generating images")
image_generation(model,batch_size,latent_dim,device,image_channels,folder)
print("IMAGE GENERATION COMPLETE")

##############################################################################################
#
#                           Section 4:  Latent Space Combination *WORK IN PROGRESS*
#
###############################################################################################

# if(training == 'LV'):
#     print(f'full image reconstruction')
#     #use the last entries of z_all as a new input
#     #get the last 4 arrays of z_all
#     z_lv = torch.zeros((batch_size,50))
#     z_lv[:,34:50] = z_all[-batch_size,:]
#     print(f'z_lv shape: {z_lv.shape}')
#     lr, epochs, batch_size = meta_data.get_training_background_setup()
#     kernel_size, init_channels, image_channels, latent_dim,dropout = meta_data.get_multiclass_model_setup()
#     model = model_combined.ConvVAE_combined(z_lv,batch_size).to(device)
    
#     training_data = get_multichannel_data(12,False)
#     test_data = get_multichannel_testdata(12,False)

#     train_loss, valid_loss, dce_score_training, dce_score_validation,z_all,folder = train_model(model,training_data,test_data,epochs,lr,batch_size,device,latent_dim,image_channels, z_lv)