import torch
import meta_data
import matplotlib.pyplot as plt
import os
import numpy as np


def image_generation(model,batch_size,latent_dim,device,image_channels, folder):
    """ Generates images from the VAE and saves them in the output folder.
    Inputs:
    - model: VAE model
    - batch_size: batch size
    - latent_dim: latent dimension
    - device: device to run the model on
    - image_channels: number of channels of the images
    - folder: folder number
    
    Output: no return value, saves the generated images in the output folder"""
    mean,std = meta_data.get_image_generation_parameter()
    z = torch.normal(mean,std, size = (batch_size, latent_dim)).to(device)
    generated_images = model.decode_from_latentspace(z)
    generated_images = generated_images.cpu().detach().numpy()
    # save images in generated images to the current folder with title as image index


    if(image_channels>1):
        generated_images = model.decode_from_latentspace(z)
        save_generated_images(generated_images,folder)

    #Only single channel LV
    else:
        for i in range(generated_images.shape[0]):
            fig = plt.figure(figsize=(10, 10))

            plt.imshow(generated_images[i,0,:,:])
            plt.title(f' Generated LV Mask, image{i}')
            plt.savefig(f'./outputs/output{folder}/generated_images{i}.jpg')
            plt.close(fig)
            plt.close('all')
            plt.clf()

def save_generated_images(images, folder):
    """ Saves the generated images from the VAE in the output folder.
    Inputs:
    - images: generated images from the VAE
    - folder: folder number
    
    Output: no return value, saves the generated images in the output folder"""


    os.makedirs(f'./outputs/output{folder}/generated_images', exist_ok=True)
    batch_size = images.shape[0]
    final_image = torch.zeros((batch_size,256,256))
    channels= images.shape[1]
    
    print(f'channels are: {channels}')
    for j in range(batch_size):
        os.makedirs(f'./outputs/output{folder}/generated_images/image{j}', exist_ok=True)
        for i in range(channels):
            #to add up all the channels simply multipy the values with the number of the channel
            #round values of the images to 0 or 1
            print(f'adding up final image, values: {j}, {i}')
            ### ROUND on torch only!
            final_image[j,:,:] = np.add(( torch.round((images[j,i,:,:])).cpu().detach().numpy()) * (i+1)  ,final_image[j,:,:])
            #print(f'recon_images_unsqueezed shape: {recon_images_unsqueezed.shape}')
            
    images = images.cpu().detach().numpy()
    #Ploting indvidual channels
    for k in range (batch_size):
        fig =plt.figure(figsize=(batch_size*2, batch_size*2))
        
        for channel in range(channels):
            plt.subplot(6, 6, channel+1)
            plt.imshow(images[k,channel,:,:])
            plt.axis('off')
            plt.title(f'Channel {channel} of image{k}')
            print(f'added channel {channel} for image {k}')
        #save figure
        fig.tight_layout()
        print(f'saving figure in folder: {folder}, image {k}, of all channels')
        plt.savefig(f'./outputs/output{folder}/generated_images/image{k}/channels.jpg')
        #close figure and free memory
        plt.close(fig)
        plt.close('all')
        plt.clf()


    ## plot the final image
    for f in range(batch_size):
        fig = plt.figure(figsize=(10, 10))
        plt.style.use('classic')
        plt.imshow(final_image[f])
        plt.title(f'Generated image {f}')
        print(f'saving final figure in folder: {folder}, image {f}, final image')
        plt.savefig(f'./outputs/output{folder}/generated_images/image{f}/final_image{f}_generated.jpg')
        plt.close(fig)
        plt.close('all')
        plt.clf()