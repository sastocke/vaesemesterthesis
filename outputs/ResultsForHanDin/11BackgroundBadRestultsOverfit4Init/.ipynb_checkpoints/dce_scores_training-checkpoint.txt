 Training data 
Channel 0, Right Ventricle Blood Pool: 0.8770170211791992
Channel 1, Left Ventricle Blood Pool: 0.8315758109092712
Channel 2, Right Ventricle Myocardium: 0.01325109414756298
Channel 3, Liver: 0.9807075262069702
Channel 4, Lung: 0.956573486328125
Channel 5, Stomach: 0.9089084267616272
Channel 6, Epicardial Fat: 0.6844340562820435
Channel 7, Surface Fat & Skin: 0.916353702545166
Channel 8, Bones (Ribs): 0.8094614148139954
Channel 9, Outside Body: 0.9044304490089417
Channel 10, Other: 0.9094534516334534
