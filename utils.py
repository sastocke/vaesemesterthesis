import imageio
import numpy as np
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
from torchvision.utils import save_image
to_pil_image = transforms.ToPILImage()
import meta_data
import os
import torch

def image_to_vid(images,folder):
    """
    Converting VAE reconstructions to PIL image format to be saved as a .gif file, shwocasing the learnning of the shapes in the images

    Inputs:
    - images: list containing the reconstructed images
    - folder: current folder where the images will be saved

    Outputs: no return value, but images will be saved in the current folder as generated_images.gif
    
    """
    imgs = [np.array(to_pil_image(img)) for img in images]
    imageio.mimsave(f'./outputs/output{folder}/generated_images.gif', imgs)


def save_reconstructed_images(recon_images, epoch,folder):

    """ 
    Saves the reconstructed images in the current folder for each epoch
    Inputs:
    - recon_images: list containing the reconstructed images
    - epoch: current epoch
    - folder: current folder where the images will be saved

    Outputs: no return value, but images will be saved in the current folder as reconstructions{epoch}.jpg
    """
    save_image(recon_images.cpu(), f"./outputs/output{folder}/reconstructions{epoch}.jpg")
    
    



def save_reconstructed_multi_images(recon_images,original_images, epoch,folder,batch_size):
    """
    List containing the reconstructed images. NumPy arrays are saved as .gif file 
    Also static images that are reconstructed by the VAE neural network will be saved.

    Inputs:
    - recon_images: list containing the reconstructed images
    - original_images: list containing the validation images that are tried to be reconstructed
    - epoch: current epoch
    - folder: current folder where the images will be saved
    - batch_size: batch size used for the training

    Outputs: no return value, but images will be saved in the current folder as reconstructions{epoch}.jpg, calls both save_image and plot_final_image
    """
    
    final_image = torch.zeros((batch_size,256,256))
    channels= recon_images.size(dim=1)
    print(f'channels are: {channels}')
    for j in range(batch_size):
        for i in range(channels):
            os.makedirs(f'./outputs/output{folder}/epoch{epoch}', exist_ok=True)
            #to add up all the channels simply multipy the values with the number of the channel
            #round values of the images to 0 or 1
            print(f'adding up final image, values: {j}, {i}')
            final_image[j,:,:] = np.add(( torch.round((recon_images[j,i,:,:])).cpu().detach().numpy()) * (i+1)  ,final_image[j,:,:])
            overlay_images(recon_images[:,i,:,:],original_images[:,i,:,:], batch_size,epoch, folder, True,i)
            #print(f'recon_images_unsqueezed shape: {recon_images_unsqueezed.shape}')
            save_channel_image(recon_images[:,i,:,:], batch_size,epoch, folder,i)
    plot_final_image(final_image,folder, epoch,batch_size)

def channel_color(channel):
    """ Set the color scheme for the different channels"""
    if channel == 0:
        plt.set_cmap(cmap='Reds')
    elif channel == 1:
        plt.set_cmap(cmap='cool')
    elif channel == 2:
        plt.set_cmap(cmap='ocean')
    elif channel == 3:
        plt.set_cmap(cmap='spring')
    elif channel == 4:
        plt.set_cmap(cmap='Greens')
    elif channel == 5:
        plt.set_cmap(cmap='copper')
    elif channel == 6:
        plt.set_cmap(cmap='Purples')
    elif channel == 7:
        plt.set_cmap(cmap='YlGnBu') 
    elif channel == 8:
        plt.set_cmap(cmap='Blues')
    elif channel == 9:
        plt.set_cmap(cmap='brg')
    elif channel == 10:
        plt.set_cmap(cmap='binary')
    elif channel == 11:
        plt.set_cmap(cmap= '')

def channel_color_plot(channel):
    """ Set the color scheme for the different channels"""
    if channel == 0:
        return 'tab:red'
    elif channel == 1:
        return 'tab:pink'
    elif channel == 2:
        return 'tab:blue'
    elif channel == 3:
        return 'yellow'
    elif channel == 4:
        return 'tab:green'
    elif channel == 5:
        return 'tab:brown'
    elif channel == 6:
        return 'tab:orange'
    elif channel == 7:
        return 'tab:purple'
    elif channel == 8:
        return 'midnightblue'
    elif channel == 9:
        return 'yellowgreen'
    elif channel == 10:
        return 'black'

def channel_name(channel):
    """ Set the name of the channels"""
    if channel == 0:
        return 'Right Ventricle Blood Pool'
    elif channel == 1:
        return 'Left Ventricle Blood Pool'
    elif channel == 2:
        return 'Right Ventricle Myocardium'
    elif channel == 3:
        return 'Liver'
    elif channel == 4:
        return 'Lung'
    elif channel == 5:
        return 'Stomach'
    elif channel == 6:
        return 'Epicardial Fat'
    elif channel == 7:
        return 'Surface Fat & Skin'
    elif channel == 8:
        return 'Bones (Ribs)'
    elif channel == 9:
        return 'Outside Body'
    elif channel == 10:
        return 'Other'
    elif channel == 11:
        return ' '

    
def save_channel_image(images, batch_size,epoch, folder,channel):
    """ Plot the images for each channel in a different color set with the channel name {channel }in the title of the plot
    
    Inputs:
    - images: list containing the images
    - batch_size: batch size used for the training
    - epoch: current epoch
    - folder: current folder where the plot will be saved
    - channel: current channel that is plotted
    
    Outputs: no return value, a plot will be saved in the current folder under the current epoch folder as channel{i}.jpg with the coloring scheme for the different channels"""

    #plot the images for each channel in a different color set with the channel name i in the title of the plot
    images = images.cpu().detach().numpy()
    fig =plt.figure(figsize=(batch_size*2, batch_size*2))

    #set the channel color scheme
    channel_color(channel)

        
    for j in range (batch_size):
        # plot the recon images and the original true labels over them with a transparency of 0.5 in green
        plt.subplot(int(batch_size/2), int(batch_size/2), j+1)
        plt.imshow(images[j,:,:])
        plt.axis('off')
        plt.title(f'Channel {channel} at epoch {epoch}')

 
    #save figure
    fig.tight_layout()
    plt.savefig(f'./outputs/output{folder}/epoch{epoch}/channel{channel}.jpg',bbox_inches='tight')
    #close figure and free memory
    plt.close(fig)
    plt.close('all')
    plt.clf()
           



def save_loss_plot(train_loss, valid_loss, folder):
    """ 
    Creates a plot of the loss from the training and validation and saves it in the current folder
    Inputs:
    - train_loss: list containing the loss for each epoch
    - valid_loss: list containing the loss for each epoch
    - folder: current folder where the plot will be saved
    
    Outputs: no return value, but a plot will be saved in the current folder as loss_plot.jpg
    """
    # loss plots
    plt.figure(figsize=(10, 7))
    plt.plot(train_loss, color='orange', label='train loss')
    plt.plot(valid_loss, color='red', label='validataion loss')
    print(f' validation loss {valid_loss}, train loss {train_loss}')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(f'./outputs/output{folder}/lossplot.jpg',bbox_inches='tight')

def save_dce_plot(dce, folder,train):
    """ Creates a plot of the dice score from the rounded reconstructed images and saves it in the current folder
    Inputs:
    - dce: list containing the dice score for each epoch
    - folder: current folder where the plot will be saved
    - train - boolean value to indicate if the plot is for the training or validation set
    
    Outputs: no return value, but a plot will be saved in the current folder as dce_plot.jpg
    """
    # loss plots
    print(f'dce shape: {dce.shape[0]}')
    if(dce.shape[0] == 1):
        
        plt.figure(figsize=(10, 7))
        plt.plot(dce[0], color='steelblue', label='DCE of the Left Ventricle Myocardium')
        plt.xlabel('Epochs')
        if(train):
            plt.ylabel('Dice Score Training Set')
            plt.legend()
            plt.savefig(f'./outputs/output{folder}/dceplot_train.jpg',bbox_inches='tight')
        else:
            plt.ylabel('Dice Score Validation Set')
            plt.legend()
            plt.savefig(f'./outputs/output{folder}/dceplot_val.jpg',bbox_inches='tight')
        return
    
    for i in range(dce.shape[0]):
        plt.figure(figsize=(10, 7))
        plt.plot(dce[i,:], color=channel_color_plot(i), label=f'channel {i}')
        plt.xlabel('Epochs')
        if(train):
            plt.ylabel('Dice Score Training Set')  
            plt.legend({channel_name(i)})
            plt.savefig(f'./outputs/output{folder}/dceplot_train{i}.jpg',bbox_inches='tight')
        else:
            plt.ylabel('Dice Score Validation Set')
            plt.legend({channel_name(i)})
            plt.savefig(f'./outputs/output{folder}/dceplot_val{i}.jpg',bbox_inches='tight')
    



def meta_data_used(folder):
    """ 
    Saves the meta data used for the training of the model in a text file

    Inputs:
    - folder: current folder where the text file will be saved
    
    Outputs: no return value, but a text file will be saved in the current folder as metadata.txt"""
    f = open(f'./outputs/output{folder}/metadata.txt', 'w')

    #LV meta data
    lr_LV, epochs_LV, batch_size_LV = meta_data.get_training_LV_setup()
    B = meta_data.get_engine_setup()
    kernel_size, init_channels, image_channels, latent_dim, dropout= meta_data.get_model_setup()


    #data range used during training
    training_start, training_end = meta_data.get_data_range()
    
    f.write(f' These are the used parameters for the trainining of LV: \n learning rate: {lr_LV}\n epochs: {epochs_LV}\n batch_size: {batch_size_LV}\n kernel_size: {kernel_size}\n init_channels: {init_channels}\n image_channels: {image_channels}\n latent_dim: {latent_dim}\n  dropout rate: {dropout} ')

    #Multiclass metadata
    kernel_size, init_channels, image_channels, latent_dim,dropout= meta_data.get_multiclass_model_setup()
    lr_background, epochs_background, batch_size_background = meta_data.get_training_background_setup()
    
    f.write(f'\nFor Multiclass the parameters were: \ninit_channels: {init_channels}\n image_channels: {image_channels}\n latent_dim: {latent_dim} \n learning rate: {lr_background}\n epochs: {epochs_background}\n batch_size: {batch_size_background}\n kernel_size: {kernel_size}\n dropout: {dropout}\n')
    f.write(f' \nThese are the shared parameters \n training_start: {training_start}\n training_end: {training_end}\n B: {B}\n')
    
    std, mean = meta_data.get_image_generation_parameter()

    f.write(f'\n The parameters for the image generation were sample from a distribution with: \n std: {std}\n mean: {mean}\n')
    
    f.close()




def plot_latent_representation_distribution(z_all,folder):
    """
    Plotting the latent representation distribution to visualise how well the distribution matches that of a gaussian distribution
    
    Inputs:
    - z_all: latent representation of all the coming from the encoder of the model used for training
    - folder: current folder where the plot will be saved

    Outputs: no return value, but a plot will be saved in the current folder as latentspace.jpg
    """
    z_all= z_all.cpu()
    z_all=z_all.detach().numpy()
    

    # Shape of z will come in as (batch_size, latent_dim)
    latent_dim = z_all.shape[1]
    # plot the latent representation distribution in a 2D histogram plot from matplotlib
    plt.figure(figsize=(100, 50))
    for i in range(latent_dim):
        plt.subplot(int(latent_dim/2), int(latent_dim/2), i+1)

        #skipping the first 200 interations as in the beginning the representation is not good and misplaces the view of the plot
        n, bins, patches = plt.hist(z_all[200:,i], 100, density=True, facecolor='g', alpha=0.75)
        plt.xlabel(f'Latent Variable {i+1}')
        plt.ylabel('Probability')
        plt.xlim([-3,3])
    plt.savefig(f'./outputs/output{folder}/latentspace.jpg',bbox_inches='tight')


def overlay_images(recon_images,original_images, batch_size,epoch,folder,multichannel,i):

    """ Produces an overlay of the original and reconstructed images to better visualise the reconstruction quality.
    The reconstructed images are rounded to 0 or 1, to be consistent with the dice score. 
    
    Inputs: 
    - recon_images: reconstructed images from the VAE
    - original_images: original images from the validation set
    - batch_size: batch size used for training 
    - epoch: current epoch
    - folder: folder number
    - multichannel: boolean, if the images are multichannel or not
    - i: channel number
    
    output: no return value, saves the overlay images in the output folder with the corresponding epoch number if multiclass with the channel number"""

    # reshaping to prepare for plotting
    recon_images = torch.round(recon_images)
    recon_images = recon_images.cpu().detach().numpy()

    recon_images = np.reshape(recon_images,(batch_size, 256, 256))

    original_images = original_images.cpu().detach().numpy()
    original_images = np.reshape(original_images,(batch_size, 256, 256))
    
    fig = plt.figure(figsize=(batch_size*2, batch_size*2))
    for j in range (batch_size):
        # plot the recon images and the original true labels over them with a transparency of 0.5 in green
        plt.subplot(int(batch_size/2), int(batch_size/2), j+1)
        plt.title(f'overlay of channel {i}, epoch {epoch}')
        plt.imshow(recon_images[j,:,:],cmap='gray')
        plt.imshow(original_images[j,:,:],cmap='Greens', alpha= 0.5)
        
    fig.tight_layout()
    #single channel
    if(multichannel):
        plt.savefig(f'./outputs/output{folder}/epoch{epoch}/overlay{i}.jpg',bbox_inches='tight')
      
    #multi channel
    else:
        plt.savefig(f'./outputs/output{folder}/OverlayOrigReconEpoch{epoch}.jpg',bbox_inches='tight')
    plt.close(fig)
    plt.close('all')
    plt.clf()



def plot_final_image(final_image,folder, epoch,batch_size):

    """ Plots the final image reconstructed from all the 11 channels. Each channel is multiplied by the corresponding channel number to allow for better visualisation.
    Inputs:
    - final_image: final image reconstructed from all the 11 channels
    - folder: folder number
    - epoch: current epoch
    - batch_size: batch size used for training, MUST BE EVEN INT NUMBER
    
    Output: no return value, saves the final image in the output folder with the corresponding epoch number"""
    fig = plt.figure(figsize=(batch_size*2, batch_size*2))
    plt.style.use('classic')
    for j in range(batch_size):
        
        plt.subplot(int(batch_size/2), int(batch_size/2), j+1)
        plt.imshow(final_image[j])
        plt.title(f'Final image in epoch {epoch}')
        plt.axis('off')
    
    
    fig.tight_layout()
    plt.savefig(f'./outputs/output{folder}/reconstructions{epoch}.jpg',bbox_inches='tight')
    plt.close(fig)
    plt.close('all')
    plt.clf()


            
        
        
def max_dice(dce, folder, train):
    
    if(train):
        f = open(f'./outputs/output{folder}/dce_scores_training.txt', 'w')
        f.write(f' Training data \n')
    else:
        f = open(f'./outputs/output{folder}/dce_scores_validation.txt', 'w')
        f.write(f' Validation data \n')

    if(dce.shape[0] == 1):
        f.write(f' The max dice score for the LV is: {dce[0].max().item()} \n')
        return
    
    for i in range(dce.shape[0]):
            
            f.write(f'Channel {i}, {channel_name(i)}: {dce[i].max().item()}\n')
            
    f.close()